package com.artemisoftnian.plugin;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;

// NEW FOR UNITY BANNERS
import com.unity3d.services.banners.BannerView;
import com.unity3d.services.banners.BannerErrorInfo;
import com.unity3d.services.banners.UnityBannerSize;
import com.unity3d.services.banners.view.BannerPosition;
import com.unity3d.ads.metadata.MetaData;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.R;
import android.graphics.Color;
import android.content.Context;


// NEW FOR UNITY ADS BUTTON
import android.widget.Button;

import java.lang.reflect.Method;

class ResultMessage{
	String message;
	PluginResult.Status status;
}

public class UnityAds3 extends CordovaPlugin {

	final UnityAdsListener unityAdsListener = new UnityAdsListener();
	final UnityBannerListener unityBannerListener = new UnityBannerListener();

    protected static final String TAG = "UnityAds3";
	protected CallbackContext context;

	protected String TEST_GAME_ID = "4370539"; // 1700140 for arPlacement test use game id: 2085042
	protected String TEST_VIDEO_AD_PLACEMENT_ID = "video";
	protected String TEST_REWARDED_VIDEO_AD_PLACEMENT_ID = "rewardedVideo";
	protected String TEST_BANNER_AD_PLACEMENT_ID = "Banner";
	protected String TEST_AR_AD_PLACEMENT_ID = "arPlacement";

	//private View bannerView; //OLD WAY

	private RelativeLayout bannerViewLayout;
	private BannerView bannerView;


	protected ResultMessage _result = new ResultMessage();
	PluginResult result = new PluginResult(PluginResult.Status.ERROR, "SOMETHING_WENT_WRONG");	


	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		//createBannerContainer();
	}
	
	@Override
	public void onPause(boolean multitasking) {
		super.onPause(multitasking);
	}
	
	@Override
	public void onResume(boolean multitasking) {
		super.onResume(multitasking);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}	

    @Override
    public boolean execute(final String action, final JSONArray args, final CallbackContext callbackContext) {
	   
		final UnityAds3 self = this;
		context = callbackContext;

		Log.d(TAG, String.format("%s, %s", "EXECUTE", action ) );
		Log.d(TAG, String.format("%s, %s", "ARGUMENTS: ", args ) );

		cordova.getActivity().runOnUiThread(() -> {
            switch (action) {
                case "UnityAdsInit":
					self.UnityAdsInit(args, callbackContext);
                    break;
                case "ShowVideoAd":
					self.ShowVideoAd(args,callbackContext);
                    break;
                case "ShowBannerAd":
					self.ShowBannerAd(args, callbackContext);
                    break;
                case "GetPlacementState":
					self.GetPlacementState(args, callbackContext);
					break;
				case "ManualGdprOpts":
					self.ManualGdprOpts(args, callbackContext);
					break;
				case "ManualCcpaOpts":
					self.ManualCcpaOpts(args, callbackContext);
					break;	
				case "ManualCustomAgeGates":
					self.ManualCustomAgeGates(args, callbackContext);
					break;								
                default:
                    callbackContext.error("Method not found");
                    break;
            }
        });
		return true;
	}


    private void UnityAdsInit(JSONArray args,  CallbackContext callbackContext){

		String gameId;
		String msg;
		Boolean isTest;
		Boolean isDebug;
		
		context = callbackContext;
		
		try{
			gameId = args.getString(0);
			isTest = args.getBoolean(1);
			isDebug = args.getBoolean(2);

		}catch(JSONException e){
			callbackContext.error(e.getMessage());
			return;
		}

		if(!UnityAds.isSupported()){
			callbackContext.error("Device not supported by UnityAds");
			return;
		}	

		if (gameId.equals("")){
			callbackContext.error("GameID Not Supplied");
			return;
		}

		if(gameId.equals("TEST")){
			gameId = TEST_GAME_ID;
		}

		if(UnityAds.isInitialized()){
			callbackContext.error("UnityAds already initialized");
			return;
		}

		//UnityAds Interestials or videos Initialization
		UnityAds.initialize(this.cordova.getActivity(), gameId, isTest);
		UnityAds.addListener(unityAdsListener);
		UnityAds.setDebugMode(isDebug);

		//UnityAds Banners
		PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", "UNITYADS", "INITIALIZING"));
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);

		result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", "UNITYADS", "IS_READY"));
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);


	}

	private void ShowVideoAd(JSONArray args, CallbackContext callbackContext){
		
		String videoAdPlacementId;
		String msg;
		try{
			videoAdPlacementId = args.getString(0);
		}catch(JSONException e){
			callbackContext.error( "Invalid PlacementID" );
			return;
		}

		if(videoAdPlacementId.equals("")){
			callbackContext.error("PlacementID not specified");
			return;
		}
		
		if(videoAdPlacementId.toUpperCase().equals("TEST")){
			videoAdPlacementId = TEST_VIDEO_AD_PLACEMENT_ID;
		}

		if(videoAdPlacementId.toUpperCase().equals("TEST_REWARDED")){
			videoAdPlacementId = TEST_REWARDED_VIDEO_AD_PLACEMENT_ID;
		}

		if(videoAdPlacementId.toUpperCase().equals("TEST_AR")){
			videoAdPlacementId = TEST_AR_AD_PLACEMENT_ID;
		}

		Log.d(TAG, videoAdPlacementId);

		if(UnityAds.isReady()){
			UnityAds.show(this.cordova.getActivity(), videoAdPlacementId);
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "READY");
		}else{
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "NOT_READY");
		}

		PluginResult result = new PluginResult(PluginResult.Status.OK, msg);
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
	}

	// Function taken from: http://www.java2s.com/example/java-src/pkg/com/cranberrygame/cordova/plugin/ad/admob/util-9a225.html
	public static View getView(CordovaWebView webView) {
		if (View.class.isAssignableFrom(CordovaWebView.class)) {
			return (View) webView;
		}

		try {
			Method getViewMethod = CordovaWebView.class.getMethod("getView", (Class<?>[]) null);
			if (getViewMethod != null) {
				Object[] args = {};
				return (View) getViewMethod.invoke(webView, args);
			}
		} catch (Exception e) {
		}

		return null;
	}

	private void ShowBannerAd(JSONArray args, CallbackContext callbackContext){
		
		String bannerAdPlacementId;
		String bannerPos;
		String msg;


		try{
			bannerAdPlacementId = args.getString(0);
			bannerPos = args.getString(1);
		}catch(JSONException e){
			callbackContext.error( "Invalid PlacementID" );
			return;
		}

		if(bannerAdPlacementId.equals("")){
			callbackContext.error("PlacementID not specified");
			return;
		}
		
		if(bannerAdPlacementId.equals("TEST")){
			bannerAdPlacementId = TEST_BANNER_AD_PLACEMENT_ID;
		}

		if(UnityAds.isReady()){

			//If not exist add the banner View layout
			if (bannerViewLayout == null) {
				bannerView = null;

				bannerViewLayout = new RelativeLayout(cordova.getActivity());
				RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
				bannerViewLayout.setLayoutParams(params);

				((ViewGroup) getView(webView)).addView(bannerViewLayout);
			}

			//If no banner showing add it to View if exist delete it.

			Log.d(TAG, String.format("%s %s", "BANNER POSITION:", bannerPos));

			if(bannerView == null){
				//Banner View Creation
				bannerView = new BannerView(this.cordova.getActivity(), bannerAdPlacementId, new UnityBannerSize(320, 50));
				bannerView.setListener(unityBannerListener);

				RelativeLayout.LayoutParams bannerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

				switch (bannerPos) {
					case "TOP_LEFT":
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						break;
					case "TOP_CENTER":
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
						bannerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
						break;
					case "TOP_RIGHT":
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						break;
					case "BOTTOM_LEFT":
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						break;
					case "BOTTOM_CENTER":
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
						bannerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
						break;
					case "BOTTOM_RIGHT":
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
						bannerParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						break;
					case "CENTER":
						bannerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
						bannerParams.addRule(RelativeLayout.CENTER_VERTICAL);
						break;
					default:
						callbackContext.error("Method not found");
						break;
				}

				bannerView.setLayoutParams(bannerParams);
				bannerView.load();

				bannerViewLayout.addView(bannerView);


			} else {
				bannerView.destroy();
				bannerView = null;
			}

			msg = String.format("[\"%s\",\"%s\"]", bannerAdPlacementId, "BANNER_READY");
		}else{
			msg = String.format("[\"%s\",\"%s\"]", bannerAdPlacementId, "UNITY ADS NOT READY YET");
		}

		PluginResult result = new PluginResult(PluginResult.Status.OK, msg);
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);	

	}

	private void GetPlacementState(JSONArray args, CallbackContext callbackContext)  {
		String videoAdPlacementId;
		String msg;
		UnityAds.PlacementState state;

		try{
			videoAdPlacementId = args.getString(0);
		}catch(JSONException e){
			callbackContext.error( "Invalid PlacementID" );
			return;
		}
		Log.d(TAG, videoAdPlacementId);

		if(videoAdPlacementId.toUpperCase().equals("TEST")){
			videoAdPlacementId = TEST_VIDEO_AD_PLACEMENT_ID;
		}

		if(videoAdPlacementId.toUpperCase().equals("TEST_REWARDED")){
			videoAdPlacementId = TEST_REWARDED_VIDEO_AD_PLACEMENT_ID;
		}

		if(videoAdPlacementId.toUpperCase().equals("TEST_AR")){
			videoAdPlacementId = TEST_AR_AD_PLACEMENT_ID;
		}

		state = UnityAds.getPlacementState(videoAdPlacementId);

		if(state == UnityAds.PlacementState.NOT_AVAILABLE){
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "NOT_AVAILABLE");
		}else if(state == UnityAds.PlacementState.DISABLED){
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "DISABLED");
		}else if(state == UnityAds.PlacementState.WAITING){
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "WAITING");
		}else if(state == UnityAds.PlacementState.NO_FILL){
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "NO_FILL");
		}else{
			msg = String.format("[\"%s\",\"%s\"]", videoAdPlacementId, "READY");
		}

		PluginResult result = new PluginResult(PluginResult.Status.OK, msg);
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);		

	}


	private void ManualGdprOpts(JSONArray args, CallbackContext callbackContext)  {

		Boolean consent;		
		String manualSetTo = "false";

		Context activityContext = this.cordova.getActivity().getApplicationContext();
		
		context = callbackContext;
		
		try{
			consent = args.getBoolean(0);
		}catch(JSONException e){
			callbackContext.error(e.getMessage());
			return;
		}		

		if(consent == true){
			manualSetTo = "true";
		}
		
		MetaData gdprMetaData = new MetaData(activityContext);
		gdprMetaData.set("gdpr.consent", manualSetTo );
		gdprMetaData.commit();

		PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", "UNITYADS", manualSetTo));
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
	}

	private void ManualCcpaOpts(JSONArray args, CallbackContext callbackContext)  {

		Boolean consent;		
		String manualSetTo = "false";
		Context activityContext = this.cordova.getActivity().getApplicationContext();
		
		context = callbackContext;
		
		try{
			consent = args.getBoolean(0);
		}catch(JSONException e){
			callbackContext.error(e.getMessage());
			return;
		}		

		if(consent == true){
			manualSetTo = "true";
		}
		
		MetaData privacyMetaData = new MetaData(activityContext);
		privacyMetaData.set("privacy.consent", manualSetTo );
		privacyMetaData.commit();

		PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", "UNITYADS", manualSetTo ));
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
	}

	private void ManualCustomAgeGates(JSONArray args, CallbackContext callbackContext)  {

		Boolean useroveragelimit;		
		String manualSetTo = "false";
		Context activityContext = this.cordova.getActivity().getApplicationContext();
		
		context = callbackContext;
		
		try{
			useroveragelimit = args.getBoolean(0);
		}catch(JSONException e){
			callbackContext.error(e.getMessage());
			return;
		}		

		if(useroveragelimit == true){
			manualSetTo = "true";
		}
		
		MetaData ageGateMetaData  = new MetaData(activityContext);
		ageGateMetaData.set("privacy.useroveragelimit", manualSetTo );
		ageGateMetaData.commit();

		PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", "UNITYADS", manualSetTo ));
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
	}



	/* UNITY ADS LISTENER */	
	private class UnityAdsListener implements IUnityAdsListener {

		@Override //This method is called when the specified ad placement becomes ready to show an ad campaign.
		public void onUnityAdsReady(final String placementId) {

			Log.d(TAG, String.format("%s", "onUnityAdsReady"));
			Log.d(TAG, placementId);

			PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", placementId, "READY"));
			result.setKeepCallback(true);
			context.sendPluginResult(result);
		}
		
		@Override //called at the start of video playback for the ad campaign being shown.
		public void onUnityAdsStart(String placementId) {
			Log.d(TAG, String.format("%s", "onUnityAdsStart"));
			Log.d(TAG, placementId);
	
			PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", placementId, "SHOWING"));
			result.setKeepCallback(true);
			context.sendPluginResult(result);
		}

		@Override //called after the ad placement is closed.
		public void onUnityAdsFinish(String placementId, UnityAds.FinishState state) {
			String msg = "";

			if(state == UnityAds.FinishState.COMPLETED){
				msg = String.format("[\"%s\",\"%s\"]", placementId, "COMPLETED");
			}else if(state == UnityAds.FinishState.SKIPPED){
				msg = String.format("[\"%s\",\"%s\"]", placementId, "SKIPPED");
			}else if(state == UnityAds.FinishState.ERROR){
				msg = String.format("[\"%s\",\"%s\"]", placementId, "ERROR");
			}

			Log.d(TAG, String.format("%s", "onUnityAdsFinish"));
			Log.d(TAG, msg);

			PluginResult result = new PluginResult(PluginResult.Status.OK, msg);
			context.sendPluginResult(result);
		}

		@Override  //called when an error occurs with Unity Ads. 
		public void onUnityAdsError(UnityAds.UnityAdsError error, String message) {
			String msg;	
			
			Log.d(TAG, String.format("%s", "onUnityAdsError"));
			Log.d(TAG, String.format("%s", message));
			
			if(error == UnityAds.UnityAdsError.NOT_INITIALIZED){
				msg = String.format("[\"%s\",\"%s\"]", message, "NOT_INITIALIZED");
			}else if(error == UnityAds.UnityAdsError.INITIALIZE_FAILED){
				msg = String.format("[\"%s\",\"%s\"]", message, "INITIALIZE_FAILED");
			}else if(error == UnityAds.UnityAdsError.INVALID_ARGUMENT){
				msg = String.format("[\"%s\",\"%s\"]", message, "INVALID_ARGUMENT");
			}else if(error == UnityAds.UnityAdsError.VIDEO_PLAYER_ERROR){
				msg = String.format("[\"%s\",\"%s\"]", message, "VIDEO_PLAYER_ERROR");
			}else if(error == UnityAds.UnityAdsError.INIT_SANITY_CHECK_FAIL){
				msg = String.format("[\"%s\",\"%s\"]", message, "INIT_SANITY_CHECK_FAIL");
			}else if(error == UnityAds.UnityAdsError.AD_BLOCKER_DETECTED){
				msg = String.format("[\"%s\",\"%s\"]", message, "AD_BLOCKER_DETECTED");
			}else if(error == UnityAds.UnityAdsError.FILE_IO_ERROR){
				msg = String.format("[\"%s\",\"%s\"]", message, "FILE_IO_ERROR");
			}else if(error == UnityAds.UnityAdsError.DEVICE_ID_ERROR){
				msg = String.format("[\"%s\",\"%s\"]", message, "DEVICE_ID_ERROR");
			}else if(error == UnityAds.UnityAdsError.SHOW_ERROR){
				msg = String.format("[\"%s\",\"%s\"]", message, "SHOW_ERROR");
			}															
			else{
				msg = String.format("[\"%s\",\"%s\"]", message, "INTERNAL_ERROR");
			}

			PluginResult result = new PluginResult(PluginResult.Status.OK, msg);
			result.setKeepCallback(true);
			context.sendPluginResult(result);				
	
		}

	}

	// UNITY BANNER LISTENER:
	private class UnityBannerListener implements BannerView.IListener {

		@Override
		public void onBannerLoaded(BannerView bannerAdView) {
			// When the banner content loads, add it to the view hierarchy:
			Log.d(TAG, String.format("%s", "onUnityBannerLoaded, banner placed"));

			PluginResult result = new PluginResult(PluginResult.Status.OK, String.format("[\"%s\",\"%s\"]", "placementId???", "BANNER LOADED"));
			result.setKeepCallback(true);
			context.sendPluginResult(result);
		}


        @Override
		public void onBannerFailedToLoad(BannerView bannerAdView, BannerErrorInfo errorInfo) {
            Log.d("SupportTest", "Banner Error" + errorInfo.errorMessage);
			
			Log.d(TAG, String.format("%s", "onBannerFailedToLoad"));
			Log.d(TAG, String.format("%s, %s", "banner error msg:", errorInfo.errorMessage));

			PluginResult result = new PluginResult(PluginResult.Status.OK, errorInfo.errorMessage);
			result.setKeepCallback(true);
			context.sendPluginResult(result);				
        }

        @Override
        public void onBannerClick(BannerView bannerAdView) {
			// Called when a banner is clicked.
			Log.d(TAG, String.format("%s", "onBannerClick, Banner Clicked"));
			bannerAdView.destroy();
			bannerView = null;
        }

        @Override
        public void onBannerLeftApplication(BannerView bannerAdView) {
			// Called when the banner links out of the application.
			Log.d(TAG, String.format("%s", "onBannerLeftApplication, Banner Left Application"));	
        }

	}


}