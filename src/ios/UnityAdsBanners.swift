import UnityAds

class UADSBannerAds: CDVPlugin, UADSBannerViewDelegate
{
    
    var promise: CommandPromise?
    var bannerView: UADSBannerView!
    var isShowing = Bool()
   
    init(prom: CommandPromise){
        promise = prom
        self.isShowing = false

        super.init()

    }
    
    func showBanner(_ webView: UIView?,_ bannerAdPlacementId: String,_ bannerPos: String, _ prom: CommandPromise){
        
        promise = prom
        
        if UnityAds.isReady(bannerAdPlacementId) {
             
             if(self.isShowing){
                self.isShowing = self.unLoadBanner(self.bannerView)
             }
             
             // Instantiate a banner view object with Placement ID and size:
             self.bannerView = UADSBannerView(placementId: bannerAdPlacementId, size: CGSize(width: 320, height: 50))
            
             //Set the banner delegate for event callbacks:
             self.bannerView.delegate = self
             
             // Add the banner view object to the view hierarchy:
             self.addBannerView(self.bannerView, bannerPos, webView)
             
             // Load ad content to the banner view object:
             self.bannerView.load()
             
             self.isShowing = true;
             
             promise?.resolve(msg: String(format: "[\"%@\",\"%@\"]", bannerAdPlacementId, "BANNER_READY") )
         }
         else{
             promise?.resolve(msg: String(format: "[\"%@\",\"%@\"]", bannerAdPlacementId, "UNITY ADS NOT READY YET") )
         }
        
    }
    
    func unLoadBanner(_ bannerView: UIView?) -> Bool{
        // Remove the banner view object from the view hierarchy:
        bannerView?.removeFromSuperview()
        return false
    }
    
    func addBannerView(_ bannerView: UIView?,_ bannerPos: String,_ view: UIView?) {
       
        bannerView?.translatesAutoresizingMaskIntoConstraints = false
        
        if let bannerView = bannerView {
            view?.addSubview(bannerView)
        }
        
        if #available(iOS 11.0, *){
            debugPrint("[UnityAds3][Banners][ITS IOS 11 OR NEWER...]")
            self.setBannerPositionSafeArea(bannerPos, view: view!)
        }
        else{
            self.setBannerPosition(bannerPos, view: view!)
        }
   
    }
    
    //MARK: - Set banner position
    @available (iOS 11, *)
    func setBannerPositionSafeArea(_ bannerPos: String, view: UIView){
        let guide = view.safeAreaLayoutGuide
     
        switch bannerPos{
            case "TOP_LEFT":
                 NSLayoutConstraint.activate([
                     guide.topAnchor.constraint(equalTo:bannerView.topAnchor),
                     guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor)
                 ])
            case "TOP_CENTER":
                 NSLayoutConstraint.activate([
                     guide.topAnchor.constraint(equalTo:bannerView.topAnchor),
                     guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
                     guide.rightAnchor.constraint(equalTo:bannerView.rightAnchor)
                 ])
            case "TOP_RIGHT":
                 NSLayoutConstraint.activate([
                     guide.topAnchor.constraint(equalTo:bannerView.topAnchor),
                     guide.rightAnchor.constraint(equalTo:bannerView.rightAnchor)
                 ])
            case "BOTTOM_LEFT":
                 NSLayoutConstraint.activate([
                     guide.bottomAnchor.constraint(equalTo:bannerView.bottomAnchor),
                     guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor)
                 ])
            case "BOTTOM_CENTER":
                 NSLayoutConstraint.activate([
                     guide.bottomAnchor.constraint(equalTo:bannerView.bottomAnchor),
                     guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
                     guide.rightAnchor.constraint(equalTo:bannerView.rightAnchor)
                 ])
            case "BOTTOM_RIGHT":
                 NSLayoutConstraint.activate([
                     guide.bottomAnchor.constraint(equalTo:bannerView.bottomAnchor),
                     guide.rightAnchor.constraint(equalTo:bannerView.rightAnchor)
                 ])
             case "CENTER":
                NSLayoutConstraint.activate([
                    guide.centerYAnchor.constraint(equalTo:bannerView.centerYAnchor),
                    guide.centerXAnchor.constraint(equalTo:bannerView.centerXAnchor)
                ])
             default:
                NSLayoutConstraint.activate([
                    guide.bottomAnchor.constraint(equalTo:bannerView.bottomAnchor),
                    guide.leftAnchor.constraint(equalTo: bannerView.leftAnchor),
                    guide.rightAnchor.constraint(equalTo:bannerView.rightAnchor)
                ])

         }
         
     }
    
    func setBannerPosition(_ bannerPos: String, view: UIView) {
        var bannerPosVal: [NSLayoutConstraint]
        
        switch bannerPos{
            case "TOP_LEFT":
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0)
                ]
                
            case "TOP_CENTER":
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                ]
            case "TOP_RIGHT":
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0)
                ]
            case "BOTTOM_LEFT":
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0),
                    NSLayoutConstraint(item: bannerView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1.0, constant: 0)
                ]
            case "BOTTOM_CENTER":
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0),
                    NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
                ]
            case "BOTTOM_RIGHT":
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0),
                    NSLayoutConstraint(item: bannerView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0)
                ]
            case "CENTER":
               bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0),
                    NSLayoutConstraint(item: bannerView, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
               ]
            default:
                bannerPosVal = [
                    NSLayoutConstraint(item: bannerView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: 0),
                    NSLayoutConstraint(item: bannerView, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
            ]
        }
        
        view.addConstraints(bannerPosVal)
    }
    
    
    
    // BANNERADS EVENTS - Implement the delegate methods:
    // MARK: : UADSBannerViewDelegate
         
     func bannerViewDidLoad(_ bannerView: UADSBannerView) {
         debugPrint("[UnityAds3][Banners][bannerViewDidLoad]", bannerView)

         if let nPromise = promise {
             nPromise.resolve(msg:  "BANNER_DID_LOAD" )
             //promise = nil
         }
         
     }
     
     func bannerViewDidClick(_ bannerView: UADSBannerView!) {
         debugPrint("[UnityAds3][Banners][unityAdsBannerDidClick]")
         
         if let nPromise = promise {
             nPromise.resolve(msg: String(format: "[\"%@\",\"%@\"]", bannerView.placementId, "BANNER_DID_CLICK") )
             //promise = nil
         }
         
         self.isShowing = self.unLoadBanner(bannerView)
         
     }
     
     func bannerViewDidLeaveApplication(_ bannerView: UADSBannerView!) {
         
         debugPrint("[UnityAds3][Banners][unityAdsBannerDidLeaveApplication]")
         
         if let nPromise = promise {
             nPromise.resolve(msg: "BANNER_DID_LEAVE_APPLICATION" )
             //promise = nil
         }

     }
     
     func bannerViewDidError(_ bannerView: UADSBannerView!, error: UADSBannerError!) {
         debugPrint("[UnityAds3][unityAdsBannerDidError]", error)
         
         if let nPromise = promise {
             nPromise.resolve(msg:  String(format: "[\"%@\",\"%@\"]", error, "BANNER_DID_ERROR") )
             //promise = nil
         }
     }
    
    
}
