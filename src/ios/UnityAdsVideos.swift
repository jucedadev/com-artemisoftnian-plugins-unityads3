import UIKit
import UnityAds


class UnityVideoAds: CDVPlugin,  UnityAdsShowDelegate, UnityAdsInitializationDelegate, UnityAdsDelegate
{


    //var plugin: CDVPlugin?
    var promise: CommandPromise?
    var callbackId: String?
    var delegate: CDVCommandDelegate?
    
    init(gameID: String, testMode: Bool, isDebug: Bool, prom: CommandPromise){
        
        promise = prom
         
        debugPrint("[UnityAds3][Videos Init]", promise ?? "PROMISE:NULL", prom)
        
        super.init()
        
        UnityAds.initialize(gameID, testMode: testMode, initializationDelegate: self)
        UnityAds.add(self)
        UnityAds.setDebugMode(isDebug)
    }
    
    
    func show(viewController: UIViewController, placementId: String,  prom: CommandPromise ){
        
        //if promise != nil{
         //  prom.resolve(msg: String(format: "[\"%@\",\"%@\"]", placementId, "UNITYADS_IS_BUSY"))
           //return
        //}
        
        promise = prom
        callbackId = prom.callback
        
        UnityAds.show(viewController, placementId: placementId, showDelegate: self)
        
    }
    
    
    //NEW VIDEOADS SHOW EVENTS - Implement the delegate methods:
    //MARK: : UnityAdsShowDelegate
    
    func unityAdsShowComplete(_ placementId: String, withFinish state: UnityAdsShowCompletionState) {
        
        debugPrint("[UnityAds3][unityAdsShowComplete]")
        
        if let nPromise = self.promise {
            if state == .showCompletionStateSkipped{
                debugPrint("[UnityAds3][unityAdsShowComplete]", placementId, "SKIPPED")
                nPromise.resolve(msg:  String(format: "[\"%@\",\"%@\"]", placementId, "SKIPPED") )
                promise = nil
             }
            else if state == .showCompletionStateCompleted {
                 debugPrint("[UnityAds3][unityAdsShowComplete]", placementId, "COMPLETED")
                 nPromise.resolve(msg:  String(format: "[\"%@\",\"%@\"]", placementId, "COMPLETED"))
                 promise = nil
             }
        }

    }
    
    func unityAdsShowFailed(_ placementId: String, withError error: UnityAdsShowError, withMessage message: String) {
        debugPrint("[UnityAds3][unityAdsShowFailed]", placementId, message )
        
        if let nPromise = promise {
            if error == .showErrorAlreadyShowing{
               nPromise.resolve(msg:  "SHOW_ERROR_ALREADY_SHOWING")
            }
            else if error == .showErrorInvalidArgument {
                nPromise.resolve(msg:  "SHOW_ERROR_INVALID_ARGUMENT")
            }
            else if error == .showErrorNoConnection {
                nPromise.resolve(msg:  "SHOW_ERROR_NO_CONNECTION")
            }
            else if error == .showErrorNotReady {
                nPromise.resolve(msg:  "SHOW_ERROR_NOT_READY")
            }
            else if error == .showErrorVideoPlayerError {
                nPromise.resolve(msg:  "SHOW_ERROR_VIDEO_PLAYER_ERROR")
            }
            else {
               nPromise.resolve(msg:  "SHOW_ERROR_INTERNAL_ERROR")
            }
            
            promise = nil

        }
    }
    
    func unityAdsShowStart(_ placementId: String) {
        debugPrint("[UnityAds3][unityAdsShowStart]", placementId)
        
        if let nPromise = promise {
            nPromise.resolve(msg: String(format: "[\"%@\",\"%@\"]", placementId, "SHOWING"))
        }
    }
    
    func unityAdsShowClick(_ placementId: String) {
        debugPrint("[UnityAds3][unityAdsShowClick]", placementId)
        if let nPromise = promise {
            nPromise.resolve(msg: String(format: "[\"%@\",\"%@\"]", placementId, "SHOWN_AD_CLICK"))
        }
    }
    
    //NEW VIDEOADS INITIALIZATION EVENTS - Implement the delegate methods:
    //MARK: UnityAdsInitializationDelegate

    func initializationComplete() {
        debugPrint("[UnityAds3][initializationComplete][unityAdsReady]")
        if let nPromise = promise {
            nPromise.resolve(msg: String(format: "[\"%@\",\"%@\"]", "UNITYADS", "READY"))
        }
    }
    
    func initializationFailed(_ error: UnityAdsInitializationError, withMessage message: String) {
        debugPrint("[UnityAds3][initializationFailed]")
        
        if let nPromise = promise {
            
            debugPrint("[UnityAds3][unityAdsDidError]", message)
            
            if error == .initializationErrorAdBlockerDetected {
               nPromise.resolve(msg:  "INIT_FAIL_AD_BLOCKER_DETECTED")
            }
            else if error == .initializationErrorInvalidArgument {
                nPromise.resolve(msg:  "INIT_FAIL_INVALID_ARGUMENT")
            }
            else {
               nPromise.resolve(msg:  "INIT_FAIL_INTERNAL_ERROR")
            }
            
            promise = nil

        }
    }
    
    
    //VIDEOADS EVENTS [DEPRECIATED METHODS]:
    //MARK: : UnityAdsDelegate    
    func unityAdsReady(_ placementId: String) { }
     
    func unityAdsDidStart(_ placementId: String) { }
     
    func unityAdsDidError(_ error: UnityAdsError, withMessage message: String) {
       /*
        if let nPromise = promise {
            
            debugPrint("[UnityAds3][unityAdsDidError]", error, message)
            
            if error == .notInitialized {
               nPromise.resolve(msg:  "NOT_INITIALIZED")
            }
            else if error == .initializedFailed {
                nPromise.resolve(msg:  "INITIALIZE_FAILED")
            }
            else {
               nPromise.resolve(msg:  "INTERNAL_ERROR")
            }
            
            promise = nil

        }
        */
        
    }
    
    func unityAdsDidFinish(_ placementId: String, with state: UnityAdsFinishState) {

        //debugPrint("[UnityAds3][unityAdsDidFinish][Depreciated Call]")
        
        /*
        
        if let nPromise = promise {
            
            if state == .skipped{
                debugPrint("[UnityAds3][unityAdsDidFinish]", placementId, "SKIPPED")
                nPromise.resolve(msg:  String(format: "[\"%@\",\"%@\"]", placementId, "SKIPPED") )
             }
            else if state == .completed {
                 debugPrint("[UnityAds3][unityAdsDidFinish]", placementId, "COMPLETED")
                 nPromise.resolve(msg:  String(format: "[\"%@\",\"%@\"]", placementId, "COMPLETED"))
             }
            else if state == .error {
                debugPrint("[UnityAds3][unityAdsDidFinish]", placementId, "ERROR")
                nPromise.resolve(msg: String(format: "[\"%@\",\"%@\"]", placementId, "ERROR"))
            }
            
            promise = nil
            
        }
       
        */
        
        

    }
    
}
