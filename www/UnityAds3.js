    var exec = require('cordova/exec');
    
    var CLASS_NAME = 'UnityAds3'; //changed PLUGIN_NAME TO CLASS_NAME
    
    // METHODS
    exports.UnityAdsInit = function UnityAdsInit(gameId, isTest, isDebug, fn) {
    
        var METHOD_NAME = "UnityAdsInit"; 
        var args = [gameId, isTest, isDebug]; 
   
        function success (str){
            if(str == "INIT_SUCESS"){
                self.unity_ads_state.initialized = true;
            }
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
    
        exec(success, error, CLASS_NAME, METHOD_NAME, args); 
    };

    exports.ShowVideoAd = function ShowVideoAd(videoAdPlacementId, fn) {
    
        var METHOD_NAME = "ShowVideoAd"; 
        var args = [videoAdPlacementId];  
    
        function success (str){
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
        exec(success, error, CLASS_NAME,METHOD_NAME,args);  
    
    };
    
    exports.ShowBannerAd = function ShowBannerAd(bannerAdPlacementId, bannerPosition,  fn) {    
        var METHOD_NAME = "ShowBannerAd"; 
        var args = [bannerAdPlacementId, bannerPosition];  
    
        function success (str){
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
        exec(success, error, CLASS_NAME,METHOD_NAME,args);    
    };
    
    exports.GetPlacementState = function GetPlacementState(videoAdPlacementId, fn) {
    
        var METHOD_NAME = "GetPlacementState"; 
        var args = [videoAdPlacementId];  
    
        function success (str){
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
    
        exec(success, error, CLASS_NAME,METHOD_NAME,args);      
    };
    
    exports.ManualGdprOpts = function ManualGdprOpts( consent, fn) {
    
        var METHOD_NAME = "ManualGdprOpts"; 
        var args = [consent]; 
    
        function success (str){
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
    
        exec(success, error, CLASS_NAME, METHOD_NAME, args); 
    };

    exports.ManualCcpaOpts = function ManualCcpaOpts( consent, fn) {
    
        var METHOD_NAME = "ManualCcpaOpts"; 
        var args = [consent]; 
    
        function success (str){
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
    
        exec(success, error, CLASS_NAME, METHOD_NAME, args); 
    };

    exports.ManualCustomAgeGates = function ManualCustomAgeGates( useroveragelimit, fn) {
    
        var METHOD_NAME = "ManualCustomAgeGates"; 
        var args = [useroveragelimit]; 
    
        function success (str){
            fn(null, str);
        }
        function error (str){
            fn(str, null);
        } 
    
        exec(success, error, CLASS_NAME, METHOD_NAME, args); 
    };
