const child_process = require("child_process");
const fs = require("fs");
const path = require("path");


function podUpdate (ctx) {
    if (!ctx.opts.platforms.includes('ios'))
        return;

    return new Promise((resolve, reject) => {
        console.log("Running manual pod update");
        child_process.exec("pod update", { cwd: path.join(ctx.opts.projectRoot, "platforms/ios/") }, (err, stdout, stderr) => {
            if (err)
            {
                console.log("Pod update failed");
                if (stdout)
                    console.log(stdout);
                if (stderr)
                    console.log(stderr);
                reject();
            }
            else {
                console.log("Pod update sucess - lets get ready to rumble!");
                resolve();
            }
        });
    });
}

function androidXUpgrade (ctx) {
    if (!ctx.opts.platforms.includes('android'))
        return;
        
    const enableAndroidX = "android.useAndroidX=true";
    const enableJetifier = "android.enableJetifier=true";
    //For Cordova
    const gradlePropertiesPath = "./platforms/android/gradle.properties";
    //For PhoneGap
    const gradlePropertiesPathPG = "./platforms/android/project.properties";

    let gradleProperties =  fs.existsSync(gradlePropertiesPath)?
                            fs.readFileSync(gradlePropertiesPath, "utf8"):
                            fs.readFileSync(gradlePropertiesPathPG, "utf8");

    if (gradleProperties)
    {
        const isAndroidXEnabled = gradleProperties.includes(enableAndroidX);
        const isJetifierEnabled = gradleProperties.includes(enableJetifier);

        if (isAndroidXEnabled && isJetifierEnabled)
            return;

        if (isAndroidXEnabled === false)
            gradleProperties += "\n" + enableAndroidX;

        if (isJetifierEnabled === false)
            gradleProperties += "\n" + enableJetifier;

        fs.writeFileSync(gradlePropertiesPath, gradleProperties);
    }
}

module.exports = function (ctx) {
    androidXUpgrade(ctx);
    return podUpdate(ctx);
};
